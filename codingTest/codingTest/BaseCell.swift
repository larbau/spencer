//
//  BaseCell.swift
//  codingTest
//
//  Created by Nathan Larson on 10/24/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import UIKit

class BaseCell: UITableViewCell {
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame = newFrame
            frame.origin.x += 60.0
            frame.size.width -= 120.0
            super.frame = frame
        }
    }
}
