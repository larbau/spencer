//
//  CoreDataStack.swift
//  codingTest
//
//  Created by Nathan Larson on 10/24/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import CoreData

class CoreDataStack {
    
    typealias Action = () -> ()
    var action: Action? = { }
    
    static let shared = CoreDataStack()
    
    fileprivate var modelName: String!
    
    lazy var mainContext: NSManagedObjectContext = {
        return self.storeContainer.viewContext
    }()
    
    lazy var storeContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: self.modelName)
        container.loadPersistentStores { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Could not load stores.")
            }
        }
        
        return container
        
    }()
    
    func initializeCoreDataStack(modelName: String) -> Void {
        self.modelName = modelName
    }
    
}

extension CoreDataStack {
    
    func saveContext(completed: Action?) -> Void {
        
        guard mainContext.hasChanges else {
            completed?()
            return
        }
        
        do {
            try mainContext.save()
            completed?()
        }catch let error as NSError {
            // Couldn't save the context.
            completed?()
            print(error)
        }
        
    }
    
    func privateContext() -> NSManagedObjectContext {
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.parent = CoreDataStack.shared.mainContext
        return context
    }
    
    func childContext() -> NSManagedObjectContext {
        let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.parent = CoreDataStack.shared.mainContext
        return context
    }
    
}

